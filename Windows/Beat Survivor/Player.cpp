#include "Player.h"

Player::Player(float x, float y, float w, float h, PathNode* path):Sprite(x, y, w, h, ImageManager::gameSS)
{
	this->timePath = new TimePath(path);
	this->rotation = 0;
}

Player::~Player()
{
	delete timePath;
}

int Player::update(int timeElapsed)
{
	bool hitMissed = timePath->update(timeElapsed);
	PathNode* currentPath = timePath->getCurrentPath();
	//Move to center coordinate
	x += w / 2;
	y += h / 2;
	if (currentPath->getDestinationX() != destinationX || currentPath->getDestinationY() != destinationY)
	{
		this->startX = x;
		this->startY = y;
		this->distanceX = currentPath->getDestinationX() - x;
		this->distanceY = currentPath->getDestinationY() - y;
		this->destinationX = currentPath->getDestinationX();
		this->destinationY = currentPath->getDestinationY();
		this->rotation = currentPath->getRotation();
		//SDL_Log("ROTATION: %f", rotation);
	}
	int timeDifference = timeElapsed - currentPath->getTime();
	if (timeDifference < 0)
	{
		float progress = (timePath->getTimeBetweenNodes() + timeDifference) / (timePath->getTimeBetweenNodes()*1.0f);
		x = startX + (progress*distanceX);
		y = startY + (progress*distanceY);
	}
	else
	{
		x = currentPath->getDestinationX();
		y = currentPath->getDestinationY();
	}
	//Move from center coordinate
	x -= w / 2;
	y -= h / 2;
	if (hitMissed)
	{
		return MISS;
	}
	else
	{
		return IDLE;
	}
}

void Player::draw(SDL_Renderer* renderer, SDL_Rect* camera)
{
	timePath->draw(renderer, camera);
	spriteSheet->setRegion(128, 0, 128, 128);
	spriteSheet->draw(renderer, camera, x*factor, y*factor, w*factor, h*factor, rotation+90);
	//SDL_Log("PLAYER: %f, %f", x, y);
}