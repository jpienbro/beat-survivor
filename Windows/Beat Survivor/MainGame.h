#ifndef MAINGAME_H
#define MAINGAME_H

#include "GameScreen.h"
#include "ScoreProcessor.h"
#include "Player.h"


class MainGame : public GameScreen
{
private:
	ScoreProcessor* sp;
	Player* player;
	long timeElapsed;
	long startTime;
	float cameraX, cameraY;

public:
	MainGame();
	void init();
	void reset();
	virtual int onPress(int x2, int y2);
	virtual int onDrag(int x2, int y2);
	virtual int onRelease(int x2, int y2);
	virtual int onHold(int x2, int y2);
	virtual void start();
	virtual void update();
	virtual void draw(SDL_Renderer* renderer);
	void drawBackground(SDL_Renderer* renderer);
};
#endif