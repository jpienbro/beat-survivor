#include "GameScreen.h"

GameScreen::GameScreen()
{
	this->factor = Size::FACTOR;
	this->drawRegion = new SDL_Rect();
	this->camera = new SDL_Rect();
	camera->w = Size::WIDTH;
	camera->h = Size::HEIGHT;
}

int GameScreen::onPress(int x2, int y2)
{
	return 0;
}

int GameScreen::onDrag(int x2, int y2)
{
	return 0;
}

int GameScreen::onRelease(int x2, int y2)
{
	return 0;
}

int GameScreen::onHold(int x2, int y2)
{
	return 0;
}

void GameScreen::start()
{
}

void GameScreen::update()
{
}

void GameScreen::draw(SDL_Renderer* renderer)
{
}

void GameScreen::drawTexture(SDL_Renderer* renderer, SDL_Rect* camera, SDL_Texture* texture, float x2, float y2, float w2, float h2)
{
	drawRegion->x = (int)(x2 - camera->x);
	//Invert y-axis
	drawRegion->y = (int)((Size::HEIGHT - y2 - h2) + camera->y);
	drawRegion->w = (int)w2;
	drawRegion->h = (int)h2;
	SDL_RenderCopy(renderer, texture, NULL, drawRegion);
}