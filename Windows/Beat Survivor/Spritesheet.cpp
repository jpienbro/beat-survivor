#include "Spritesheet.h"

SpriteSheet::SpriteSheet(SDL_Texture* image)
{
	this->image = image;
	this->imageRegion = new SDL_Rect();
	this->drawRegion = new SDL_Rect();
}

SpriteSheet::~SpriteSheet()
{
	SDL_DestroyTexture(image);
	image = NULL;
	delete imageRegion;
	delete drawRegion;
	SDL_Log("SpriteSheet Destroyed");
}

void SpriteSheet::setAlpha(float alpha)
{
	SDL_SetTextureAlphaMod(image, (int)(alpha*255));
}

void SpriteSheet::setRegion(int x2, int y2, int w2, int h2)
{
	imageRegion->x = x2;
	imageRegion->y = y2;
	imageRegion->w = w2;
	imageRegion->h = h2;
}

void SpriteSheet::draw(SDL_Renderer* renderer, float x2, float y2, float w2, float h2)
{
	drawRegion->x = (int)x2;
	//Invert y-axis
	drawRegion->y = (int)(Size::HEIGHT - y2 - h2);
	drawRegion->w = (int)w2;
	drawRegion->h = (int)h2;
	SDL_RenderCopy(renderer, image, imageRegion, drawRegion);
}

void SpriteSheet::draw(SDL_Renderer* renderer, SDL_Rect* camera, float x2, float y2, float w2, float h2)
{
	drawRegion->x = (int)(x2-camera->x);
	//Invert y-axis
	drawRegion->y = (int)((Size::HEIGHT - y2 - h2) + camera->y);
	drawRegion->w = (int)w2;
	drawRegion->h = (int)h2;
	SDL_RenderCopy(renderer, image, imageRegion, drawRegion);
}

void SpriteSheet::draw(SDL_Renderer* renderer, SDL_Rect* camera, float x2, float y2, float w2, float h2, double rotation)
{
	drawRegion->x = (int)(x2 - camera->x);
	//Invert y-axis
	drawRegion->y = (int)((Size::HEIGHT - y2 - h2) + camera->y);
	drawRegion->w = (int)w2;
	drawRegion->h = (int)h2;
	SDL_RenderCopyEx(renderer, image, imageRegion, drawRegion, rotation, NULL, SDL_FLIP_NONE);
}