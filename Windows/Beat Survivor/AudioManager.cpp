#include "AudioManager.h"

std::vector<Mix_Chunk*>* AudioManager::soundEffects = NULL;
Mix_Music* AudioManager::backgroundMusic = NULL;
bool AudioManager::muteSFX = false;
bool AudioManager::muteBGM = false;
int AudioManager::currentBGM = 0;