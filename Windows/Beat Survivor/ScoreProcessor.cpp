#include "ScoreProcessor.h"

ScoreProcessor::ScoreProcessor()
{
	this->spriteSheet = ImageManager::scoreSS;
	this->combo = 0;
	this->score = 0;
	this->factor = Size::FACTOR;
}

void ScoreProcessor::reset()
{
	this->combo = 0;
	this->score = 0;
}

void ScoreProcessor::increaseCombo(int rating)
{
	combo++;
	switch (rating)
	{
		case PERFECT: score += (300+combo); break;
		case GOOD: score += (200+combo); break;
		case OK: score += (100+combo); break;
	}
	this->currentRating = rating;

	this->showRating = true;
	this->showRatingTime = SDL_GetTicks();
	this->ratingY = (Size::HEIGHT / (factor*1.0f)) / 2.0f;
}

void ScoreProcessor::resetCombo()
{
	//Note missed, reset combo
	combo = 0;
	this->currentRating = MISS;

	this->showRating = true;
	this->showRatingTime = SDL_GetTicks();
	this->ratingY = (Size::HEIGHT / (factor/30.0f)) / 2.0f;
}

void ScoreProcessor::update()
{
	if (showRating)
	{
		long timeDifference = SDL_GetTicks() - showRatingTime;
		float height = ((timeDifference - 500)*(timeDifference - 500)) / -200000.0f;
		ratingY = ((Size::HEIGHT / (factor / 30.0f)) / 1.5f) + height;
		if (timeDifference >= 1000)
		{
			showRating = false;
		}
	}
}

void ScoreProcessor::draw(SDL_Renderer* renderer)
{
	drawScore(renderer);
	if (showRating)
	{
		drawRating(renderer);
	}
}

void ScoreProcessor::drawScore(SDL_Renderer* renderer)
{
	std::string scoreString = static_cast<std::ostringstream*>(&(std::ostringstream() << score))->str();
	int length = scoreString.length();
	float width = (factor / 15.0f);
	float height = (factor / 15.0f) / 3.0f*4.0f;
	for (int i = 0; i < length; i++)
	{
		char c = scoreString[i];
		switch (c)
		{
			case '0': spriteSheet->setRegion(0, 0, 48, 64); break;
			case '1': spriteSheet->setRegion(48, 0, 48, 64); break;
			case '2': spriteSheet->setRegion(96, 0, 48, 64); break;
			case '3': spriteSheet->setRegion(144, 0, 48, 64); break;
			case '4': spriteSheet->setRegion(192, 0, 48, 64); break;
			case '5': spriteSheet->setRegion(240, 0, 48, 64); break;
			case '6': spriteSheet->setRegion(288, 0, 48, 64); break;
			case '7': spriteSheet->setRegion(336, 0, 48, 64); break;
			case '8': spriteSheet->setRegion(384, 0, 48, 64); break;
			case '9': spriteSheet->setRegion(432, 0, 48, 64); break;
		}
		spriteSheet->draw(renderer, i*width, Size::HEIGHT - height, width, height);
	}
}

void ScoreProcessor::drawRating(SDL_Renderer* renderer)
{
	switch (currentRating)
	{
		case PERFECT: spriteSheet->setRegion(0, 64, 256, 64); break;
		case GOOD: spriteSheet->setRegion(256, 64, 256, 64); break;
		case OK: spriteSheet->setRegion(0, 128, 256, 64); break;
		case MISS: spriteSheet->setRegion(0, 192, 256, 64); break;
	}
	spriteSheet->draw(renderer, (Size::WIDTH / 2) - 4 * (factor / 30.0f), ratingY*(factor / 30.0f),
		8 * (factor / 30.0f), 2 * (factor / 30.0f));

	//Combo
	drawCombo(renderer);
}

void ScoreProcessor::drawCombo(SDL_Renderer* renderer)
{
	std::string comboString = static_cast<std::ostringstream*>(&(std::ostringstream() << combo))->str();
	int length = comboString.length();
	float width = (8.0f / 256 * 48)*1.25f;
	float height = 2.5f;
	for (int i = 0; i < length; i++)
	{
		char c = comboString[i];
		switch (c)
		{
		case '0': spriteSheet->setRegion(0, 0, 48, 64); break;
		case '1': spriteSheet->setRegion(48, 0, 48, 64); break;
		case '2': spriteSheet->setRegion(96, 0, 48, 64); break;
		case '3': spriteSheet->setRegion(144, 0, 48, 64); break;
		case '4': spriteSheet->setRegion(192, 0, 48, 64); break;
		case '5': spriteSheet->setRegion(240, 0, 48, 64); break;
		case '6': spriteSheet->setRegion(288, 0, 48, 64); break;
		case '7': spriteSheet->setRegion(336, 0, 48, 64); break;
		case '8': spriteSheet->setRegion(384, 0, 48, 64); break;
		case '9': spriteSheet->setRegion(432, 0, 48, 64); break;
		}
		spriteSheet->draw(renderer, ((Size::WIDTH / 2) - (length / 2.0f*width*(factor / 30.0f))) + (i*width*(factor / 30.0f)),
			(ratingY - height)*(factor / 30.0f), width*(factor / 30.0f), height*(factor / 30.0f));
	}
}
