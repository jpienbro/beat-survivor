#include "Sprite.h"

Sprite::Sprite(float x, float y, float w, float h, SpriteSheet* spriteSheet)
{
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->spriteSheet = spriteSheet;
	this->factor = Size::FACTOR;
}

bool Sprite::contains(float x2, float y2)
{
	return (x2 >= x && x2 <= x + w && y2 >= y && y2 <= y + h);
}

bool Sprite::intersects(float x2, float y2, float w2, float h2)
{
	return (x < x2 + w2 && x + w > x2 && y < y2 + h2 && y + h > y2);
}

bool Sprite::intersects(Sprite* otherSprite)
{
	float x2 = otherSprite->x;
	float y2 = otherSprite->y;
	float w2 = otherSprite->w;
	float h2 = otherSprite->h;
	return (x < x2 + w2 && x + w > x2 && y < y2 + h2 && y + h > y2);
}

int Sprite::update(int timeElapsed)
{
	return 0;
}

void Sprite::draw(SDL_Renderer* renderer, SDL_Rect* camera)
{
}