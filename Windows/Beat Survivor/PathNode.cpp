#include "PathNode.h"

PathNode::PathNode(float destinationX, float destinationY, long time):Sprite(x, y, w, h, ImageManager::gameSS)
{
	this->destinationX = destinationX;
	this->destinationY = destinationY;
	this->time = time;
	this->duration = time;
	this->w = 0.1f;
	this->h = 0.1f;
	this->x = destinationX - (w / 2);
	this->y = destinationY - (h / 2);
	this->alpha = 0;
	this->passed = false;
	this->nextNode = NULL;
}

PathNode::~PathNode()
{
	delete nextNode;
}

PathNode* PathNode::setNextNode(PathNode* nextNode)
{
	this->nextNode = nextNode;

	float xLength = nextNode->destinationX - destinationX;
	float yLength = nextNode->destinationY - destinationY;
	double angle = 0;
	if (xLength == 0)
	{
		if (nextNode->destinationY > destinationY)
		{
			angle = 90;
		}
		else
		{
			angle = -90;
		}
	}
	else
	{
		angle = atan2(yLength, xLength);
		//To degrees
		angle *= (180 / M_PI);
	}
	if (angle < 0)
	{
		angle += 360;
	}
	angle = 360 - angle;
	this->nextNode->initActionBox(angle);
	this->duration = nextNode->time - time;

	return this->nextNode;
}

void PathNode::initActionBox(double rotation)
{
	this->rotation = rotation;

	//this.setBounds(x*Size.FACTOR, y*Size.FACTOR, w*Size.FACTOR, h*Size.FACTOR);
	//this.setOrigin((w / 2)*Size.FACTOR, (h / 2)*Size.FACTOR);
	//this.setRotation(rotation);

}

int PathNode::update(int timeElapsed)
{
	int timeDifference = timeElapsed - time;
	//Alpha value increases as time left till PathNode time decreases
	if (timeDifference >= 0)
	{
		alpha = 1;
		if (nextNode)
		{
			nextNode->update(timeElapsed);
		}
	}
	else if (timeDifference > -3000)
	{
		alpha = (timeDifference + 3000) / (3000.0f);
		if (nextNode)
		{
			nextNode->update(timeElapsed);
		}
	}
	return 0;
}

void PathNode::draw(SDL_Renderer* renderer, SDL_Rect* camera)
{
	if (alpha > 0)
	{
		spriteSheet->setAlpha(alpha);
		spriteSheet->setRegion(128, 128, 128, 128);
		spriteSheet->draw(renderer, camera, x*factor, y*factor, w*factor, h*factor);
		spriteSheet->setAlpha(1);
		if (nextNode)
		{
			nextNode->draw(renderer, camera);
		}
	}

}
