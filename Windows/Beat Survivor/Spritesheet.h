#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include "SDL.h"
#include "SDL_image.h"
#include "GameValues.h"

class SpriteSheet
{
	private:
	SDL_Texture* image;
	SDL_Rect* imageRegion;
	SDL_Rect* drawRegion;

	public:
	SpriteSheet(SDL_Texture* image);
	~SpriteSheet();
	void setAlpha(float alpha);
	void setRegion(int x2, int y2, int w2, int h2);
	void draw(SDL_Renderer* renderer, float x2, float y2, float w2, float h2);
	void draw(SDL_Renderer* renderer, SDL_Rect* camera, float x2, float y2, float w2, float h2);
	void draw(SDL_Renderer* renderer, SDL_Rect* camera, float x2, float y2, float w2, float h2, double rotation);
};
#endif