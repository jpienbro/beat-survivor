#ifndef SPRITE_H
#define SPRITE_H

#include "SDL.h"
#include <math.h>
#include "GameValues.h"
#include "ImageManager.h"

class Sprite
{
	protected:
		float x, y, w, h;
		int factor;
		SpriteSheet* spriteSheet;

	public:
		Sprite(float x, float y, float w, float h, SpriteSheet* spriteSheet);
		bool contains(float x2, float y2);
		bool intersects(float x2, float y2, float w2, float h2);
		bool intersects(Sprite* otherSprite);
		virtual int update(int timeElapsed);
		virtual void draw(SDL_Renderer* renderer, SDL_Rect* camera);

		//Getters and Setters
		float getX()const{ return x; }
		float getY()const{ return y; }
		float getW()const{ return w; }
		float getH()const{ return h; }
		void setX(float x){ this->x = x; }
		void setY(float y){ this->y = y; }
		void setW(float w){ this->w = w; }
		void setH(float h){ this->h = h; }
};
#endif