#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include "SDL.h"
#include "SDL_mixer.h"
#include <string>
#include <vector>

class AudioManager
{
public:
	static std::vector<Mix_Chunk*>* soundEffects;
	static Mix_Music* backgroundMusic;
	static bool muteSFX;
	static bool muteBGM;
	static int currentBGM;
};

static void initAudio()
{
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096);
	AudioManager::soundEffects = new std::vector<Mix_Chunk*>();
	AudioManager::soundEffects->push_back(Mix_LoadWAV("sounds/scratch.wav"));
	SDL_Log("AUDIO LOADED");
}

static void playSound(int index)
{
	if (!AudioManager::muteSFX && index < AudioManager::soundEffects->size())
	{
		Mix_PlayChannel(-1, AudioManager::soundEffects->at(index), 0);
	}
}


static void pauseBackgroundMusic()
{
	if (AudioManager::backgroundMusic && Mix_PlayingMusic())
	{
		Mix_PauseMusic();
	}
}

static void resumeBackgroundMusic()
{
	if (AudioManager::backgroundMusic && Mix_PausedMusic())
	{
		Mix_ResumeMusic();
	}
}

static void stopBackgroundMusic()
{
	if (AudioManager::backgroundMusic && Mix_PlayingMusic())
	{
		Mix_HaltMusic();
		Mix_FreeMusic(AudioManager::backgroundMusic);
		AudioManager::backgroundMusic = NULL;
		AudioManager::currentBGM = -1;
	}
}

static void playBackgroundMusic(int id)
{
	if (id != AudioManager::currentBGM)
	{
		if (AudioManager::backgroundMusic)
		{
			stopBackgroundMusic();
		}
		if (!AudioManager::muteBGM)
		{
			AudioManager::currentBGM = id;
			switch (id)
			{
				case 1: AudioManager::backgroundMusic = Mix_LoadMUS("music/music_1.ogg"); break;
			}
			Mix_PlayMusic(AudioManager::backgroundMusic, -1);
		}
	}
}

static void clearAudio()
{
	for (int i = 0; i < AudioManager::soundEffects->size(); i++)
	{
		Mix_FreeChunk(AudioManager::soundEffects->at(i));
	}
	AudioManager::soundEffects->clear();
	delete AudioManager::soundEffects;
	if (AudioManager::backgroundMusic)
	{
		Mix_FreeMusic(AudioManager::backgroundMusic);
	}
	SDL_Log("AUDIO CLEARED");
}
#endif
