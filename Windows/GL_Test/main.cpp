#include "SDL.h"
#include "GameProcessor.h"

#if defined(__IPHONEOS__) || defined(__ANDROID__)
#define HAVE_OPENGLES
#endif
#ifdef HAVE_OPENGLES
#include "SDL_opengles.h"
#else
#include "SDL_opengl.h"
#endif

#define GAME_LOOP 10

GameProcessor* gp;
SDL_TimerID timerID;
SDL_Window* window;
SDL_GLContext glContext;
Uint32 lastUpdate;

void initGame()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_RETAINED_BACKING, 0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	//Create the window
	window = SDL_CreateWindow("Beat Survivor SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		800, 480, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	glContext = SDL_GL_CreateContext(window);
	int w = 0;
	int h = 0;
	SDL_GetWindowSize(window, &w, &h);
	Size::WIDTH = w;
	Size::HEIGHT = h;
	Size::FACTOR = Size::HEIGHT;
	glOrtho(0.0, (w*1.0)/h, 0.0, 1.0, -1.0, 1.0);

	//Init images and audio
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, NULL);
	initImages(renderer);
	initAudio();
	playBackgroundMusic(1);

	//Init GameProcessor
	gp = new GameProcessor();

	lastUpdate = SDL_GetTicks();
}

void draw(float x, float y, SDL_Texture* texture)
{
	float w, h;
	SDL_GL_BindTexture(texture, &w, &h);
	w = 0.5;
	h = 0.5;
	SDL_Log("Texture: %f, %f", w, h);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex2f(x, y);
	glTexCoord2f(1.0, 0.0);
	glVertex2f(x + w, y);
	glTexCoord2f(1.0, 1.0);
	glVertex2f(x + w, y + h);
	glTexCoord2f(0.0, 1.0);
	glVertex2f(x, y + h);
	glEnd();
	SDL_GL_UnbindTexture(texture);
	glLoadIdentity();
}

void runGame()
{
	Uint8 done = 0;
	SDL_Event event;
	while (!done)
	{
		//Check Input
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_KEYDOWN || event.type == SDL_FINGERDOWN)
			{
				playSound(0);
			}
			else if (event.type == SDL_QUIT)
			{
				done = 1;
				SDL_Log("QUIT");
			}
		}
		//Update 
		//gp->update();

		//Draw
		glClear(GL_COLOR_BUFFER_BIT);
		/*glColor3f(0.7, 0.5, 0.8);
		glRectf(0.0, 0.0, 0.5, 0.5);*/
		draw(0.1, 0.0, ImageManager::background);
		SDL_GL_SwapWindow(window);

		//Aim for 60 fps
		int sleep = (int)(16 - (SDL_GetTicks() - lastUpdate));
		lastUpdate = SDL_GetTicks();
		SDL_Log("LAST UPDATE: %d", lastUpdate);
		if (sleep > 0)
		{
			SDL_Log("SLEEP: %d", sleep);
			SDL_Delay(sleep);
		}
	}
}

void cleanup()
{
	SDL_RemoveTimer(timerID);
	SDL_GL_DeleteContext(glContext);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

int main(int argc, char *argv[])
{
	initGame();
	runGame();
	cleanup();
	return 0;
}
