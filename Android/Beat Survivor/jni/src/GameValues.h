#ifndef GAMEVALUES_H
#define GAMEVALUES_H

//Size
class Size
{
	public:
	static int FACTOR;
	static int WIDTH;
	static int HEIGHT;
};

enum Directions
{
	LEFT = 10,
	RIGHT = 11,
	UP = 12,
	DOWN = 13
};

enum Ratings
{
	PERFECT = 20,
	GOOD = 21,
	OK = 22,
	IDLE = 23,
	MISS = 24
};

#endif
