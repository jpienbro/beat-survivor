#ifndef GAMEPROCESSOR_H
#define GAMEPROCESSOR_H

class GameScreen;

#include "SDL.h"
#include "GameScreen.h"
#include "GameValues.h"
#include "ImageManager.h"
#include "MainGame.h"

class GameProcessor
{
	protected:
		GameScreen* gameScreen;
		GameScreen* nextScreen;

	public:
        GameProcessor();
		void initGame();
		void endGame();
		void pause();
		void resume();
		int onPress(int x2, int y2);
		int onDrag(int x2, int y2);
		int onRelease(int x2, int y2);
		int onHold(int x2, int y2);
		int onBackPressed();
		void check();
        void update();
		void draw(SDL_Renderer* renderer);
};
#endif
