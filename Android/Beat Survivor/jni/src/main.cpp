#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "SDL.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "GameProcessor.h"
#include <string>

#if defined(__IPHONEOS__) || defined(__ANDROID__)
#define HAVE_OPENGLES
#endif
#ifdef HAVE_OPENGLES
#include "SDL_opengles.h"
#else
#include "SDL_opengl.h"
#endif


int main(int argc, char *argv[])
{
	SDL_Log("INITIALIZE");

	SDL_Window *window;
	SDL_Renderer *renderer;

	SDL_Init(SDL_INIT_VIDEO);
	SDL_Init(SDL_INIT_AUDIO);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);
	SDL_GL_SetAttribute(SDL_GL_RETAINED_BACKING, 0);

	if (SDL_CreateWindowAndRenderer(0, 0, 0, &window, &renderer) < 0)
	{
		exit(2);
	}
	int w = 0;
	int h = 0;
	SDL_GetWindowSize(window, &w, &h);

	Size::WIDTH = w;
	Size::HEIGHT = h;
	Size::FACTOR = Size::HEIGHT;

	SDL_Log("SCREEN: %dx%d", Size::WIDTH, Size::HEIGHT);

	initImages(renderer);
	initAudio();
	GameProcessor* gp = new GameProcessor();
	SDL_Surface* screen = SDL_GetWindowSurface(window);

	/* Main render loop */
	Uint8 done = 0;
	SDL_Event event;
	Uint32 lastUpdate = 0;
	bool touchMode = true;
	while (!done)
	{
		/* Check for events */
		while (SDL_PollEvent(&event))
		{
			if (touchMode)
			{
				if (event.type == SDL_FINGERDOWN)
				{
					playSound(0);
					int x = (int)(event.tfinger.x*Size::WIDTH);
					int y = (int)(event.tfinger.y*Size::HEIGHT);
					//SDL_Log("ON PRESS: %d, %d", x, y);
					gp->onPress(x, y);
				}
				else if (event.type == SDL_FINGERUP)
				{
					int x = (int)(event.tfinger.x*Size::WIDTH);
					int y = (int)(event.tfinger.y*Size::HEIGHT);
					//SDL_Log("ON RELEASE: %d, %d", x, y);
					gp->onRelease(x, y);
				}
				else if (event.type == SDL_FINGERMOTION)
				{
					int x = (int)(event.tfinger.x*Size::WIDTH);
					int y = (int)(event.tfinger.y*Size::HEIGHT);
					//SDL_Log("ON DRAG: %d, %d", x, y);
					gp->onDrag(x, y);
				}
				else if (event.key.keysym.sym == SDLK_AC_BACK)
				{
					done = 1;
				}
			}
			else
			{
				if (event.type == SDL_MOUSEBUTTONDOWN)
				{
					playSound(0);
					int x = (int)event.button.x;
					int y = (int)event.button.y;
					//SDL_Log("ON PRESS: %d, %d", x, y);
					gp->onPress(x, y);
				}
				else if (event.type == SDL_MOUSEBUTTONUP)
				{
					int x = (int)event.button.x;
					int y = (int)event.button.y;
					//SDL_Log("ON RELEASE: %d, %d", x, y);
					gp->onRelease(x, y);
				}
				else if (event.type == SDL_MOUSEMOTION && event.motion.state == SDL_PRESSED)
				{
					int x = (int)event.motion.x;
					int y = (int)event.motion.y;
					//SDL_Log("ON DRAG: %d, %d", x, y);
					gp->onDrag(x, y);
				}
				else if (event.type == SDL_QUIT)
				{
					done = 1;
				}
			}
		}

		//Update 
		gp->update();

		//Draw
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
		SDL_RenderClear(renderer);
		gp->draw(renderer);
		SDL_RenderPresent(renderer);

		//Aim for 60 fps
		int sleep = (int)(16 - (SDL_GetTicks() - lastUpdate));
		lastUpdate = SDL_GetTicks();
		//SDL_Log("LAST UPDATE: %d", lastUpdate);
		if (sleep > 0)
		{
			//SDL_Log("SLEEP: %d", sleep);
			SDL_Delay(sleep);
		}

	}

	exit(0);
}