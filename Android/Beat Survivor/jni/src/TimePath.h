#ifndef TIMEPATH_H
#define TIMEPATH_H

#include "PathNode.h"

class TimePath
{
private:
	PathNode* path;
	bool finished;
	int timeBetweenNodes;
	bool startNodePassed;

public:
	TimePath(PathNode* path);
	~TimePath();
	int checkHit(int timeElapsed);
	void gotoNextNode();
	bool update(int timeElapsed);
	void draw(SDL_Renderer* renderer, SDL_Rect* camera);

	//Getters and Setters
	PathNode* getCurrentPath()const{ return path; }
	int getTimeBetweenNodes()const{ return timeBetweenNodes; }
};
#endif