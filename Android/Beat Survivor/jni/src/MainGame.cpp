#include "MainGame.h"

MainGame::MainGame():GameScreen()
{
	this->spriteSheet = ImageManager::gameSS;
	this->backgroundImage = ImageManager::background;
	this->sp = new ScoreProcessor();
	init();
	start();
}

void MainGame::init()
{
	PathNode* startNode = new PathNode(0, 0, 100);
	PathNode* timeNode = startNode->setNextNode(new PathNode(0, 1, 7000));
	timeNode = timeNode->setNextNode(new PathNode(0.25f, 1.25f, 7500));
	timeNode = timeNode->setNextNode(new PathNode(0.5f, 1.5f, 8000));
	timeNode = timeNode->setNextNode(new PathNode(1.5f, 1.5f, 9900));
	timeNode = timeNode->setNextNode(new PathNode(1.75f, 1.2f, 10400));
	timeNode = timeNode->setNextNode(new PathNode(2.0f, 0.9f, 10900));
	timeNode = timeNode->setNextNode(new PathNode(3.0f, 3.0f, 17900));
	this->player = new Player(0, 0, 0.1f, 0.1f, startNode);
}

void MainGame::reset()
{
	SDL_Log("RESET");
	stopBackgroundMusic();
	this->timeElapsed = 0;
	delete player;
	cameraX = 0;
	cameraY = 0;
	camera->x = 0;
	camera->y = 0;
	init();
	start();
}

int MainGame::onPress(int x2, int y2)
{
	this->timeElapsed = (long)((SDL_GetTicks() - startTime));
	int rating = player->getTimePath()->checkHit(timeElapsed);
	switch (rating)
	{
		case PERFECT: sp->increaseCombo(rating); ("PERFECT Timing"); break;
		case GOOD: sp->increaseCombo(rating); SDL_Log("GOOD Timing"); break;
		case OK: sp->increaseCombo(rating); SDL_Log("OK Timing"); break;
		case IDLE: SDL_Log("Timing too early"); break;
	}
	return 0;
}

int MainGame::onDrag(int x2, int y2)
{
	return 0;
}

int MainGame::onRelease(int x2, int y2)
{
	return 0;
}

int MainGame::onHold(int x2, int y2)
{
	return 0;
}

void MainGame::start()
{
	playBackgroundMusic(1);
	this->startTime = SDL_GetTicks();
}

void MainGame::update()
{
	this->timeElapsed = (long)((SDL_GetTicks() - startTime));
	if (timeElapsed < 20000)
	{
		if (player->update(timeElapsed) == MISS)
		{
			SDL_Log("NOTE MISSED");
			sp->resetCombo();
		}

		//Camera
		cameraX = ((player->getX() + (player->getW() / 2))*factor) - (Size::WIDTH / 2);
		cameraY = ((player->getY() + (player->getH() / 2))*factor) - (Size::HEIGHT / 2);

		sp->update();
	}
	else
	{
		reset();
	}
}

void MainGame::draw(SDL_Renderer* renderer)
{
	camera->x = (int)cameraX;
	camera->y = (int)cameraY;
	//SDL_Log("Camera: %d, %d", camera->x, camera->y);
	//SDL_Log("Camera SIZE: %d, %d", cameraRect->w, cameraRect->h);

	drawBackground(renderer);
	player->draw(renderer, camera);

	sp->draw(renderer);
}

void MainGame::drawBackground(SDL_Renderer* renderer)
{
	float x = (float)ceil(cameraX / Size::WIDTH);
	float y = (float)ceil(cameraY / Size::WIDTH);
	//SDL_Log("X: %f, Y: %f", x, y);

	drawTexture(renderer, camera, backgroundImage, x*Size::WIDTH, y*Size::WIDTH, Size::WIDTH, Size::WIDTH);
	drawTexture(renderer, camera, backgroundImage, (x - 1)*Size::WIDTH, y*Size::WIDTH, Size::WIDTH, Size::WIDTH);
	drawTexture(renderer, camera, backgroundImage, x*Size::WIDTH, (y - 1)*Size::WIDTH, Size::WIDTH, Size::WIDTH);
	drawTexture(renderer, camera, backgroundImage, (x - 1)*Size::WIDTH, (y - 1)*Size::WIDTH, Size::WIDTH, Size::WIDTH);
}