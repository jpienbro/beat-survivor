#ifndef GAMESCREEN_H
#define GAMESCREEN_H

#include "SDL.h"
#include "GameValues.h"
#include "ImageManager.h"
#include "AudioManager.h"
#include "math.h"

class GameScreen
{
	protected:
		SpriteSheet* spriteSheet;
		SDL_Texture* backgroundImage;
		SDL_Rect* drawRegion;
		SDL_Rect* camera;
		int factor;
		bool started;

	public:
        GameScreen();
		virtual int onPress(int x2, int y2);
		virtual int onDrag(int x2, int y2);
		virtual int onRelease(int x2, int y2);
		virtual int onHold(int x2, int y2);
		virtual void start();
        virtual void update();
		virtual void draw(SDL_Renderer* renderer);
		void drawTexture(SDL_Renderer* renderer, SDL_Rect* camera, SDL_Texture* texture, float x2, float y2, float w2, float h2);
};
#endif
