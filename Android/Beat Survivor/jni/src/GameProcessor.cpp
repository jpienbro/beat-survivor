#include "GameProcessor.h"

GameProcessor::GameProcessor()
{
	initGame();
}

void GameProcessor::initGame()
{
	this->gameScreen = new MainGame();
}

void GameProcessor::endGame()
{

}

void GameProcessor::pause()
{

}

void GameProcessor::resume()
{

}

int GameProcessor::onPress(int x2, int y2)
{
	return gameScreen->onPress(x2, y2);
}

int GameProcessor::onDrag(int x2, int y2)
{
	return gameScreen->onDrag(x2, y2);
}

int GameProcessor::onRelease(int x2, int y2)
{
	return gameScreen->onRelease(x2, y2);
}

int GameProcessor::onHold(int x2, int y2)
{
	return gameScreen->onHold(x2, y2);
}

int GameProcessor::onBackPressed()
{
	//Android version only
	return 0;
}

void GameProcessor::check()
{

}

void GameProcessor::update()
{
	gameScreen->update();
}

void GameProcessor::draw(SDL_Renderer* renderer)
{
	gameScreen->draw(renderer);
}