#ifndef PATHNODE_H
#define PATHNODE_H

#include "Sprite.h"

class PathNode: public Sprite
{
	protected:
		float destinationX, destinationY;
		long time;
		double rotation;
		PathNode* nextNode;
		bool passed;

		float alpha;
		int duration;

	public:
		PathNode(float destinationX, float destinationY, long time);
		~PathNode();
		PathNode* setNextNode(PathNode* nextNode);
		void initActionBox(double rotation);
		virtual int update(int timeElapsed);
		virtual void draw(SDL_Renderer* renderer, SDL_Rect* camera);

		//Getters and Setters
		float getDestinationX()const{ return destinationX; }
		float getDestinationY()const{ return destinationY; }
		long getTime()const{ return time; }
		double getRotation()const{ return rotation; }
		PathNode* getNextNode()const{ return nextNode; }
		int getDuration()const{ return duration; }
		bool isPassed()const{ return passed; }
		void setPassed(bool passed){ this->passed = passed; }
};
#endif