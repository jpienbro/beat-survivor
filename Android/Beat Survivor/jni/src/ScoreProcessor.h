#ifndef SCOREPROCESSOR_H
#define SCOREPROCESSOR_H

#include "GameValues.h"
#include "ImageManager.h"
#include "Spritesheet.h"
#include <sstream>
#include <iostream>

class ScoreProcessor
{
private:
	SpriteSheet* spriteSheet;
	int factor;
	int combo;
	long score;

	int currentRating;
	long showRatingTime;
	bool showRating;
	float ratingY;

public:
	ScoreProcessor();
	void reset();
	void increaseCombo(int rating);
	void resetCombo();
	void update();
	void draw(SDL_Renderer* renderer);
	void drawScore(SDL_Renderer* renderer);
	void drawRating(SDL_Renderer* renderer);
	void drawCombo(SDL_Renderer* renderer);
};
#endif
