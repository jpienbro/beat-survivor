#ifndef PLAYER_H
#define PLAYER_H

#include "Sprite.h"
#include "TimePath.h"

class Player : public Sprite
{
protected:
	TimePath* timePath;
	float startX, startY, distanceX, distanceY, destinationX, destinationY;
	double rotation;

public:
	Player(float x, float y, float w, float h, PathNode* path);
	~Player();
	virtual int update(int timeElapsed);
	virtual void draw(SDL_Renderer* renderer, SDL_Rect* camera);

	//Getters and Setters
	TimePath* getTimePath()const{ return timePath; }
};
#endif