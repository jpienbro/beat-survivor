#ifndef IMAGEMANAGER_H
#define IMAGEMANAGER_H

#include "SDL.h"
#include "SDL_image.h"
#include "Spritesheet.h"
#include <string>

class ImageManager
{
	public:
	static SpriteSheet* gameSS;
	static SDL_Texture* background;
	static SpriteSheet* scoreSS;
};

static SDL_Texture* loadTexture(SDL_Renderer* renderer, std::string path)
{
	SDL_Texture* texture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface != NULL)
	{
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);

		//Free the old loaded surface
		SDL_FreeSurface(loadedSurface);

		if (texture != NULL)
		{
			return texture;
		}
		else
		{
			SDL_Log("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
			return NULL;
		}
	}
	else
	{
		SDL_Log("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
		return NULL;
	}
}

static void initImages(SDL_Renderer* renderer)
{
	ImageManager::gameSS = new SpriteSheet(loadTexture(renderer, "images/basic_ss.png"));
	ImageManager::background = loadTexture(renderer, "images/background.png");
	ImageManager::scoreSS = new SpriteSheet(loadTexture(renderer, "images/score_ss.png"));
	SDL_Log("IMAGES LOADED");
}

static void clearImages()
{
	delete ImageManager::gameSS;
	SDL_DestroyTexture(ImageManager::background);
	ImageManager::background = NULL;
	SDL_Log("IMAGES CLEARED");
}
#endif
