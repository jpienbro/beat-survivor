#include "TimePath.h"

TimePath::TimePath(PathNode* path)
{
	this->path = path;
	this->finished = false;
	this->timeBetweenNodes = path->getTime();
	this->startNodePassed = false;
}

TimePath::~TimePath()
{
	delete path;
}

int TimePath::checkHit(int timeElapsed)
{
	int difference = abs(timeElapsed - path->getTime());
	if (!finished && !path->isPassed())
	{
		if (difference <= 75)
		{
			//gotoNextNode();
			path->setPassed(true);
			return PERFECT;
		}
		else if (difference <= 150)
		{
			//gotoNextNode();
			path->setPassed(true);
			return GOOD;
		}
		else if (difference <= 225)
		{
			//gotoNextNode();
			path->setPassed(true);
			return OK;
		}
		else
		{
			return IDLE;
		}
	}
	else
	{
		return IDLE;
	}
}

void TimePath::gotoNextNode()
{
	if (path->getNextNode())
	{
		timeBetweenNodes = path->getNextNode()->getTime() - path->getTime();
		path = path->getNextNode();
	}
	else
	{
		finished = true;
	}
}

bool TimePath::update(int timeElapsed)
{
	int timeOut = 0;
	bool hitMissed = false;
	if (!finished)
	{
		if (timeElapsed - path->getTime() > timeOut)
		{
			if (!path->isPassed())
			{
				if (!startNodePassed){ startNodePassed = true; }
				else
				{
					hitMissed = true;
				}
				path->setPassed(true);
			}
			gotoNextNode();
		}
		path->update(timeElapsed);
	}
	return hitMissed;
}

void TimePath::draw(SDL_Renderer* renderer, SDL_Rect* camera)
{
	path->draw(renderer, camera);
}